class AddNamespaceToHtmlBlocks < ActiveRecord::Migration
  def up
    add_content_column prefix(:html_blocks), :namespace_id, :integer

    add_index prefix(:html_blocks), :namespace_id
    add_index prefix(:html_block_versions), :namespace_id
  end

  def down
    remove_index prefix(:html_blocks), :namespace_id
    remove_index prefix(:html_block_versions), :namespace_id

    remove_column prefix(:html_blocks), :namespace_id
    remove_column prefix(:html_block_versions), :namespace_id
  end
end
