class CreateNamespaces < ActiveRecord::Migration
  def change
    create_table prefix(:namespaces) do |t|
      t.string :name
      t.belongs_to :section

      t.timestamps
    end
    add_index prefix(:namespaces), :section_id
  end
end
