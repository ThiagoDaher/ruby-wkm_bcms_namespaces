class AddNamespaceToFileBlocks < ActiveRecord::Migration
  def up
    add_content_column prefix(:file_blocks), :namespace_id, :integer

    add_index prefix(:file_blocks), :namespace_id
    add_index prefix(:file_block_versions), :namespace_id
  end

  def down
    remove_index prefix(:file_blocks), :namespace_id
    remove_index prefix(:file_block_versions), :namespace_id

    remove_column prefix(:file_blocks), :namespace_id
    remove_column prefix(:file_block_versions), :namespace_id
  end
end
