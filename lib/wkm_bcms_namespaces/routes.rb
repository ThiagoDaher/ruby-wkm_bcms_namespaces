module Cms::Routes
  def routes_for_wkm_bcms_namespaces
    namespace(:cms) do
      resources :namespaces
    end
  end
end