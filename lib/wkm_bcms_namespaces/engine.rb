require 'browsercms'
Dir[File.dirname(__FILE__) + "/../wkm/**/*.rb"].each { |file| require file }

module WkmBcmsNamespaces
  class Engine < ::Rails::Engine
    config.autoload_paths += Dir["#{config.root}/lib/wkm/**/"]

    # Model extensions
    config.to_prepare do
      Cms::Connector.send(:include, ConnectorCallbacks)
      Cms::Page.send(:include, PageCallbacks)

      ActiveRecord::Base.send(:include, NamespacedModel)
    end

    # Helper initializer
    initializer 'wkm_bcms_namespaces.action_controller' do |app|
      ActiveSupport.on_load :action_controller do
        helper WkmBcmsNamespaces::ApplicationHelper
      end
    end

    isolate_namespace WkmBcmsNamespaces
  end
end
