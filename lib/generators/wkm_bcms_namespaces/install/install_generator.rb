require 'cms/module_installation'

class WkmBcmsNamespaces::InstallGenerator < Cms::ModuleInstallation
  add_migrations_directory_to_source_root __FILE__
  
  
  def copy_migrations
    rake 'wkm_bcms_namespaces:install:migrations'
  end

  # Uncomment to add module specific seed data to a project.
  #def add_seed_data_to_project
  #  copy_file "../wkm_bcms_namespaces.seeds.rb", "db/wkm_bcms_namespaces.seeds.rb"
  #  append_to_file "db/seeds.rb", "load File.expand_path('../wkm_bcms_namespaces.seeds.rb', __FILE__)\n"
  #end
  
  def add_routes
    mount_engine(WkmBcmsNamespaces)
  end
    
end