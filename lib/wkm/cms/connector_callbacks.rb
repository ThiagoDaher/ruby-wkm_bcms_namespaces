module WkmBcmsNamespaces
  module ConnectorCallbacks
    def self.included(klass)
      klass.extend ClassMethods
      klass.portlet_route_callbacks
    end

    def reload_portlet_routes
      if self.connectable_type.eql? "Cms::Portlet"
        portlet = Cms::Portlet.find(self.connectable_id)
        if portlet.class.respond_to?(:have_route)
          # Testar se ja existe um connector desse portlet em outra pagina
          #if
          #  errors.add(:base, "Portlet already connected in other page")
          #  return false
          #end

          portlet.reload_portlet_route
        end
      end
    end

    module ClassMethods
      def portlet_route_callbacks
        before_save :reload_portlet_routes
        before_destroy :reload_portlet_routes
      end
    end
  end
end