module WkmBcmsNamespaces
  module PageCallbacks
    def self.included(klass)
      klass.extend ClassMethods
      klass.portlet_route_callbacks
    end

    # TODO: Execute a callback for the publish page action to reload the portlet routes
    def reload_portlet_routes
      puts "after_commit"
    end

    module ClassMethods
      def portlet_route_callbacks
        after_commit :reload_portlet_routes
        after_save :reload_portlet_routes

      end
    end
  end
end