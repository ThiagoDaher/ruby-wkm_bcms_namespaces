module Cms
  module Behaviors
    module InstanceMethods

      def template_path
        begin
          if !self.layout.nil? && !self.layout.empty?
            "portlets/#{self.class.name.tableize.sub('_portlets', '')}/#{self.layout}"
          else
            self.class.template_path
          end
        rescue
          self.class.template_path
        end
      end

      def perform_render(controller)
        return "Exception: #{@render_exception}" if @render_exception

        unless @controller
          # We haven't prepared to render. This should only happen when logged in, as we don't want
          # errors to bubble up and prevent the page being edited in that case.
          prepare_to_render(controller)
        end

        if self.respond_to?(:deleted) && self.deleted
          logger.error "Attempting to render deleted object: #{self.inspect}"
          msg = (@mode == 'edit' ? %Q[<div class="error">This #{self.class.name} has been deleted.  Please remove this container from the page</div>] : '')
          return msg
        end

        # Create, Instantiate and Initialize the view
        action_view = Cms::ViewContext.new(@controller, assigns_for_view)

        # Determine if this content should render from a file system template or inline (i.e. database based template)
        if respond_to?(:inline_options) && self.inline_options && self.inline_options.has_key?(:inline)
          options = self.inline_options
          locals = {}
          action_view.render(options, locals)
        else
          action_view.render(:file => template_path)
        end
      end
    end
  end
end