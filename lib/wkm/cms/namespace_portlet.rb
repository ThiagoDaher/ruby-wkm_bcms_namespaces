module WkmBcmsNamespaces
  module NamespacePortlet
    def self.included(klass)
      klass.extend ClassMethods
      klass.have_route = false
      klass.is_details = true
    end

    def namespace
      WkmBcmsNamespaces::Namespace.find(self.namespace_id) unless self.namespace_id.nil?
    end

    # Get the connectors from the last version pages associated with this portlet
    def connectors
      Cms::Connector.joins("INNER JOIN cms_pages ON cms_pages.id = cms_connectors.page_id")
                    .where("cms_connectors.page_version = cms_pages.version")
                    .where(:connectable_type => "Cms::Portlet", :connectable_id => self.id)
    end

    # Reload this portlet page routes after updating/connecting a portlet instance
    def reload_portlet_route
      if self.class.have_route
        connectors = self.connectors
        return if connectors.count <= 0

        page = self.connectors.first.page
        page_route = Cms::PageRoute.find_by_page_id(page.id)
        if self.deleted
          # Remove the page_route if the portlet is deleted
          page_route.destroy unless page_route.nil?
        else
          route_name = self.namespace.nil? ? "#{self.class.to_s.tableize.gsub(/_portlets$/, "")}" : "#{self.namespace.path}_#{self.class.to_s.tableize.gsub(/_portlets$/, "")}"
          regexp = self.class.is_details ? /\/[^\/]+\/?$/ : /\/?$/
          pattern = page.path.gsub(regexp, "") + self.class.params_path
          # Create or update the page_route, if the portlet is connected to a published page
          if page_route.nil?
            Cms::PageRoute.create!({name: route_name, pattern: pattern, page_id: page.id, code: ""})
          else
            page_route.name = route_name
            page_route.pattern = pattern
            page_route.save!
          end
        end
        # Assure to reload the routes
        Rails.application.reload_routes!
      end
    end

    protected

    # Prevent destroying of portlet instances associated with published pages
    def have_connectors
      if self.connectors.count > 0
        errors.add(:base, "Unable to delete portlet instance. Cause: Portlet instance is being used.")
        false
      end
    end

    # Prevents creating a portlet instance with the same namespace
    def same_namespace
      portlets = self.id.nil? ? self.class.all : self.class.where("id != ?", self.id)

      if portlets.delete_if{|p| p.namespace_id != self.namespace_id}.count > 0
        errors.add(:namespace_id, "is already being used by another portlet instance")
      end
    end

    module ClassMethods
      attr_accessor :have_route, :params_path, :is_details

      def have_route_with_params(params_path, options = {})
        after_validation :reload_portlet_route, :on => :update
        before_destroy :have_connectors

        validate :same_namespace

        self.have_route = true
        self.params_path = params_path
        self.is_details = options[:is_details] unless options[:is_details].nil?
      end
    end

  end
end


