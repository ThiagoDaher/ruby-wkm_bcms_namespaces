module WkmBcmsNamespaces
  module NamespacedModel
    def self.included(klass)
      klass.extend ClassMethods
    end

    module ClassMethods
      def have_wkm_namespace
        belongs_to :namespace, :class_name => "WkmBcmsNamespaces::Namespace"

        self.define_singleton_method :namespaces do
          self.select(:namespace_id).group(:namespace_id).having("namespace_id IS NOT NULL").map do |model|
            model.namespace
          end
        end
      end
    end
  end
end