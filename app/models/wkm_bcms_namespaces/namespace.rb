module WkmBcmsNamespaces
  class Namespace < ActiveRecord::Base
    self.table_name = Cms::Namespacing.prefix("namespaces")
    attr_accessible :name, :section_id

    belongs_to :section, :class_name => "Cms::Section"

    validates_presence_of :section, :name

    def path
      name.slugify.gsub(/\-/, '_')
    end
  end
end
