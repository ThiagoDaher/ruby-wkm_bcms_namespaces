module WkmBcmsNamespaces
  module ApplicationHelper
    def namespaces_drop_down(form, options={})
      options[:field_name] ||= :namespace_id
      namespaces = options[:section].nil? ? Namespace.order("name ASC") : Namespace.where(:section_id => options[:section].id).order("name ASC")

      form.cms_drop_down options[:field_name], namespaces.collect{ |ns| ["#{ns.name} (#{ns.section.name})", ns.id] }.unshift(["", ""])
    end

    # Generates a drop down form input to select the view to use in a portlet instance
    def layouts_drop_down(form, portlet_name)
      form.cms_drop_down :layout,
                      ActionController::Base.view_paths.map{ |p|
                        # Maps the portlet views directories
                        Dir["#{p}/portlets/#{portlet_name}/*"]
                      }.flatten.delete_if{ |p|
                        # Remove partials from selection
                        File.basename(p).match /^_/
                      }.map { |f|
                        view_file = File.basename(f)
                        [Cms::PageTemplate.display_name(view_file), view_file]
                      }
    end
  end
end
